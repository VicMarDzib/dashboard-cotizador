<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Panel de usuario -  cotizador online
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-2" style="min-height: 500px;">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Tus API KEYS
                </h2>
                <br>
                <div style="padding: 50px;">
                    <table>
                        <tbody id="contenidolista">

                        </tbody>
                    </table>
                </div>

                <input type="hidden" value="{{ $user_id }}" id="iduser">

                <button style="margin-left: 50px;" type="submit" onclick="addApiKey()" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition ml-4">
                    Generar nueva clave publica
                </button>
            </div>
        </div>
    </div>


    <script>
        console.log('funciona js');

        const idusuario = document.getElementById('iduser').value;

        fetch("/api/obtener_claves/" + idusuario).then(res => res.json())
            .then((res) => {
                console.log(res);
                if (res.success == true) {

                    let data = '';
                    res.data.map((elem) => {
                        data += `<tr style="padding: 30px;">
                                <td>${elem.token}</td>
                                <td>
                                    <button class="bg-gray-800 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" style="margin-left: 50px;"
                                    onclick="copiar('${elem.token}')">
                                        Copiar
                                    </button>

                                </td>
                            </tr>`;
                    });

                    document.getElementById('contenidolista').innerHTML = data;
                }
            });

        function copiar(codigo) {


            /* Copy the text inside the text field */
            navigator.clipboard.writeText(codigo);

            Swal.fire({
                icon: 'success',
                title: 'Clave copiada!',
                timer: 1000
            });
        }


        function addApiKey() {
            fetch("/api/generar_token", {
                method: 'POST',
                body: JSON.stringify({
                    user_id: idusuario
                }),
                headers: {
                    'Content-Type': 'application/json'
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
            }).then(res => res.json()).then((res) => {
                console.log(res);
                if (res.success == true) {

                    let prevhtml = document.getElementById('contenidolista').innerHTML;

                    document.getElementById('contenidolista').innerHTML = prevhtml + `
                    <tr style="padding: 30px;">
                                <td>${res.token}</td>
                                <td>
                                    <button class="bg-gray-800 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" style="margin-left: 50px;"
                                    onclick="copiar('${res.token}')">
                                        Copiar
                                    </button>

                                </td>
                            </tr>
                    `;
                }
            })
        }
    </script>
</x-app-layout>

<?php

namespace App\Http\Controllers;

use App\Models\ApiKey;
use Illuminate\Http\Request;

class ApikeyController extends Controller
{
    public function insert(Request $request)
    {
        $key = new ApiKey();
        $key->token = md5(uniqid());
        $key->status = 1;
        $key->user_id = $request->get('user_id');

        $key->save();

        return response()->json([
            'success' => true,
            'token' => $key->token
        ], 200);
    }

    public function obtenerApiKeys($id)
    {
        $data = ApiKey::where('user_id', $id)->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ], 200);
    }

    public function verificarToken($token)
    {
        $clave = ApiKey::where('token', '=', $token)->where('status', 1)->first();

        if ($clave) {
            return response()->json([
                'success' => true,
                'data' => $clave
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'data' => null
            ], 200);
        }
    }
}

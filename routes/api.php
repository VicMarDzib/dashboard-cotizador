<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/generar_token', [
    'as' => 'generar_token',
    'uses' => 'App\Http\Controllers\ApikeyController@insert'
]);


Route::get('/obtener_claves/{id}', [
    'as' => 'obtener_clave',
    'uses' => 'App\Http\Controllers\ApikeyController@obtenerApiKeys'
]);



Route::get('/verificar_clave/{token}', [
    'as' => 'obtener_clave',
    'uses' => 'App\Http\Controllers\ApikeyController@verificarToken'
]);
